# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['Requirement']


class Requirement(ModelSQL, ModelView):
    "Transport Requirement"
    __name__ = "transport.requirement"
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(Requirement, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))
