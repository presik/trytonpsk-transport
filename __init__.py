# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import classification
from . import place
from . import service
from . import driver
from . import contract
from . import requirement
from . import configuration


def register():
    Pool.register(
        requirement.Requirement,
        configuration.Configuration,
        driver.Driver,
        place.Route,
        place.RouteCost,
        contract.Objective,
        place.TransportPlace,
        classification.Classification,
        service.TransportShift,
        service.Service,
        service.ServiceCostLine,
        service.ServiceRouteLine,
        contract.TransportContract,
        contract.ContractExtract,
        contract.ContractRequirement,
        contract.ContractVehicle,
        contract.ContractExtractDriver,
        service.ServicesSummaryStart,
        contract.PrintContractExtractStart,
        service.ServicesDailyStart,
        service.DriverAccessSyncStart,
        contract.AutoGenerateExtractStart,
        driver.TestTypeDriver,
        driver.TestDriver,
        contract.ContractRoute,
        module='transport', type_='model')
    Pool.register(
        service.ServicesSummary,
        service.ServiceForceDraft,
        contract.PrintContractExtract,
        service.ServicesDaily,
        service.DriverAccessSync,
        contract.AutoGenerateExtract,
        service.CreateSaleFromService,
        module='transport', type_='wizard')
    Pool.register(
        service.ServiceReport,
        service.ServicesSummaryReport,
        contract.TransportContractReport,
        contract.PrintContractExtractReport,
        contract.PrintContractExtractMarkedReport,
        service.ServicesDailyReport,
        service.ServicesDriverReport,
        service.ServicesShiftReport,
        module='transport', type_='report')
