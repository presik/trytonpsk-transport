# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['Classification']


class Classification(ModelSQL, ModelView):
    "Classification Activity"
    __name__ = 'transport.classification'
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(Classification, cls).__setup__()
