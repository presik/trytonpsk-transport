# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval

__all__ = ['Configuration']


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Transport Configuration'
    __name__ = 'transport.configuration'
    law_resolution = fields.Char('Law Resolution')
    transport_service_sequence = fields.Many2One('ir.sequence', 'Transport Service Sequence', required=True)
    transport_contract_sequence = fields.Many2One('ir.sequence', 'Contract Transport Sequence', required=True)
