# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, timedelta

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['Driver', 'TestDriver', 'TestTypeDriver']


class Driver(ModelSQL, ModelView):
    "Transport Driver"
    __name__ = "transport.driver"
    # _rec_name = 'party'
    party = fields.Many2One('party.party', 'Party', required=True)
    code = fields.Char('Code')
    active = fields.Boolean('Active')
    fine = fields.Integer('Fine')
    registration_date = fields.Date('Registration Date')
    license_validity_date = fields.Date('License Validity Date',
        required=True)
    license_class = fields.Char('License Class')
    traffic_license_number = fields.Char('Traffic License Number',
        required=True)
    traffic_office_city = fields.Char('Traffic License City',
        required=True)
    tests = fields.One2Many('transport.test_driver',
            'driver', 'Test Lines')
    tests = fields.One2Many('transport.test_driver',
            'driver', 'Test Lines')
    notes = fields.Text('Notes')
    # notification_documents = fields.One2Many('notification.document',
    #     'origin', string='Notification Documents')
    company = fields.Many2One('company.company', 'Company', required=True)

    @classmethod
    def __setup__(cls):
        super(Driver, cls).__setup__()
        cls._order.insert(0, ('party', 'ASC'))

    def get_rec_name(self, name):
        rec_name = self.party.rec_name
        if self.code:
            rec_name = '[' + self.code + ']' + ' ' + rec_name
        return (rec_name)

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def send_document_alert(cls):
        pool = Pool()
        Template = pool.get('electronic.mail.template')
        templates = Template.search([
            ('model.model', '=', 'transport.driver'),
        ])
        if not templates:
            return

        drivers = cls.search([
            ('active', '=', True),
        ])

        today = date.today()
        alerts_to_send = []
        for template in templates:
            for driver in drivers:
                for doc in driver.documents:
                    for alert in doc.kind.alerts:
                        if alert.days == 0 or doc.date_end is None:
                            continue
                        delta = (doc.date_end - timedelta(alert.days))
                        if delta > today:
                            continue
                        elif alert.frecuency == 'once':
                            if delta == today:
                                alerts_to_send.append(doc)
                                continue
                        else:
                            alerts_to_send.append(doc)

        context = Transaction().context.copy()
        with Transaction().set_context(context):
            template.render_and_send(alerts_to_send)


class TestDriver(ModelSQL, ModelView):
    "Transport Place"
    __name__ = "transport.test_driver"
    # _rec_name = 'party'
    driver = fields.Many2One('transport.driver', 'Driver', required=True)
    test_type = fields.Many2One('transport.test_type', 'Test Type', required=True)
    date = fields.Date('Date', required=True)
    expire_days = fields.Function(fields.Integer('Expire Days'),
        'get_expire')

    def get_expire(self, name):
        res = None
        if self.date and self.test_type.expire:
            today = date.today()
            res = self.test_type.expire - (today - self.date).days
        return res


class TestTypeDriver(ModelSQL, ModelView):
    "Test Type Driver"
    __name__ = "transport.test_type"
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active')
    expire = fields.Integer('Expire', help="In days")
    prenotification_days = fields.Integer('Prenotification Days',
        help="In days")

    @staticmethod
    def default_active():
        return True
