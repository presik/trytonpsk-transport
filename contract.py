# This file is part of the transport_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateAction, StateTransition, StateView, Wizard

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module!")
    print("Please install it...!")

STATES = {
    'readonly': Eval('state') != 'draft',
}

_ZERO = Decimal('0.0')


class Objective(ModelSQL, ModelView):
    "Objective"
    __name__ = 'transport.contract.objective'
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active')
    tour = fields.Text('Tour', required=True)

    @staticmethod
    def default_active():
        return True


class TransportContract(Workflow, ModelSQL, ModelView):
    "Transport Contract"
    __name__ = 'transport.contract'
    _rec_name = 'number'
    number = fields.Char('Number', states=STATES)
    party = fields.Many2One('party.party', 'Party', required=True,
            states=STATES)
    contract_date = fields.Date('Contract Date', required=True, states=STATES)
    payment_term = fields.Many2One('account.invoice.payment_term',
            'Payment Term', states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES)
    vehicles = fields.One2Many('transport.contract.vehicle',
        'contract', 'Vehicles', states={
            'readonly': Eval('state').in_(['finished', 'canceled']),
        }, context={
            'vehicles': Eval('vehicles'),
        })
    extracts = fields.One2Many('transport.contract.extract',
        'contract', 'Extracts', states={
            'readonly': Eval('state') == 'finished',
            'required': Eval('state').in_(['finished', 'execution']),
        }, depends=['state'])
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('execution', 'Execution'),
            ('finished', 'Finished'),
            ('canceled', 'Canceled'),
            ], 'State', readonly=True)
    kind = fields.Selection([
            ('agreement', 'Agreement'),
            ('occasionally', 'Occasionally'),
            ('special', 'Special'),
            ], 'Kind', required=True, states=STATES)
    kind_string = kind.translated('kind')
    total_amount = fields.Numeric('Total Amount', digits=(16, 2),
            states=STATES)
    duration = fields.Char('Duration', states=STATES, required=True)
    start_date = fields.Date('Start Date', states=STATES, required=True)
    end_date = fields.Date('End Date', states=STATES, required=True)
    objective = fields.Many2One('transport.contract.objective', 'Objective',
        states=STATES, required=False)
    tour = fields.Text('Tour', depends=['objective'], states={
        'required': Eval('state') == 'execution',
        'readonly': Eval('state').in_(
            ['execution', 'finished', 'canceled'])})
    contact = fields.Many2One('party.party', 'Customer Contact',
            states=STATES, required=True)
    total_amount_words = fields.Function(fields.Char('Total Amount Words'),
            'get_total_amount_words')
    law_resolution = fields.Function(fields.Char('Law Resolution'),
            'get_law_resolution')
    requirements = fields.One2Many('transport.contract.requirement',
            'contract', 'Requirements', states={
            'readonly': Eval('state') == 'finished',
            'required': Eval('state').in_(['execution', 'finished']),
            })
    comments = fields.Text('Comments', states=STATES)
    itinerary = fields.Char('Itinerary', states=STATES)
    place_arrival = fields.Char('Place Arrival', states={
            'invisible': Eval('kind') != 'occasionally',
        })
    passengers_number = fields.Integer('Passengers Number', states={
            'invisible': Eval('kind') != 'occasionally',
        })
    meeting_place = fields.Char('Meeting Place', states={
            'invisible': Eval('kind') != 'occasionally',
        })
    meeting_time = fields.DateTime('Meeting Datetime', states={
            'invisible': Eval('kind') != 'occasionally',
        })
    origin = fields.Many2One('transport.place', 'Origin',
            states=STATES)
    destination = fields.Many2One('transport.place', 'Destination', states=STATES)
    resolution = fields.Char('Resolution', states={
            'readonly': False,
        })
    routes = fields.One2Many('transport.contract.route', 'contract', 'Routes',
        states=STATES)

    @classmethod
    def __setup__(cls):
        super(TransportContract, cls).__setup__()
        cls._order.insert(0, ('contract_date', 'DESC'))
        # cls._error_messages.update({
        #         'payment_term_missing': 'The payterm is missing!',
        #         'sequence_missing': 'The sequence is missing!',
        #         'requirement_not_accomplish': 'There are requirement not accomplish!',
        #         'delete_cancel': ('Transport Contract "%s" must be canceled before '
        #             'deletion.'),
        # })
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('draft', 'canceled'),
            ('confirmed', 'draft'),
            ('canceled', 'draft'),
            ('confirmed', 'execution'),
            ('execution', 'finished'),
            ('execution', 'confirmed'),
            ('finished', 'execution'),
        ))
        cls._buttons.update({
            'create_lines': {
                'invisible': ((Eval('state') != 'draft') | Eval('lines', [0])),
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'draft': {
                'invisible': ~Eval('state').in_(['canceled', 'confirmed']),
            },
            'confirm': {
                'invisible': Eval('state').in_(['canceled', 'confirmed', 'finished']),
            },
            'execution': {
                'invisible': Eval('state').in_(['canceled', 'execution', 'draft']),
            },
            'finish': {
                'invisible': Eval('state') != 'execution',
            },
        })

    @staticmethod
    def default_resolution():
        config = Pool().get('transport.configuration')(1)
        return config.law_resolution

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_contract_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @classmethod
    def delete(cls, contracts):
        # Cancel before delete
        cls.cancel(contracts)
        for contract in contracts:
            if contract.state != 'canceled':
                raise UserError('delete_cancel')
        super(TransportContract, cls).delete(contracts)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, contracts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, contracts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('execution')
    def execution(cls, contracts):
        for contract in contracts:
            contract.check_requirements()

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, contracts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, contracts):
        for contract in contracts:
            contract.set_number()

    def set_number(self):
        """
        Set sequence number
        """
        pool = Pool()
        config = pool.get('transport.configuration')(1)
        Sequence = pool.get('ir.sequence')
        if self.number:
            return
        if not config.transport_contract_sequence:
            raise UserError('sequence_missing')
        seq = config.transport_contract_sequence.get()
        self.write([self], {'number': seq})

    def get_total_amount_words(self, name=None):
        if self.total_amount and conversor:
            return (conversor.cardinal(int(self.total_amount))).upper()
        else:
            return ''
        return

    def check_requirements(self, named=None):
        for requirement in self.requirements:
            if requirement.accomplish is False:
                raise UserError('requirement_not_accomplish')

    def get_law_resolution(self, named=None):
        config = Pool().get('transport.configuration')(1)
        return config.law_resolution

    @fields.depends('objective', 'tour')
    def on_change_objective(self):
        if self.objective:
            self.tour = self.objective.tour


class ContractExtract(ModelSQL, ModelView):
    "Contract Extract"
    __name__ = "transport.contract.extract"
    _rec_name = 'sequence'
    sequence = fields.Char('Sequence', required=True)
    number = fields.Char('Number', depends=['sequence'], states={
            'readonly': True,
        })
    contract = fields.Many2One('transport.contract', 'Contract',
            ondelete='CASCADE', required=True)
    route = fields.Many2One('transport.contract.route', 'Route', domain=[
        ('contract', '=', Eval('contract')),
        ])
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    vehicle = fields.Many2One('transport.contract.vehicle', 'Vehicle',
        required=True, domain=[
            ('contract', '=', Eval('contract')),
        ], depends=['contract'])
    drivers = fields.Many2Many('transport.contract.extract-transport.driver',
        'extract', 'driver', 'Drivers', required=True)
    tour = fields.Char('Tour')
    notes = fields.Char('Notes')

    @classmethod
    def __setup__(cls):
        super(ContractExtract, cls).__setup__()
        cls._order.insert(0, ('sequence', 'DESC'))

    @fields.depends('sequence', 'number', '_parent_contract.number',
        '_parent_contract.resolution')
    def on_change_sequence(self):
        self.number = None
        if self.sequence and self.contract and self.contract.number and self.contract.resolution:
            sequence_ = self.sequence
            if len(self.sequence) > 4:
                sequence_ = self.sequence[-4:]
            self.number = self.contract.resolution + self.contract.number + sequence_


class ContractVehicle(ModelSQL, ModelView):
    "Contract Vehicle"
    __name__ = "transport.contract.vehicle"
    # _rec_name = 'equipment'
    contract = fields.Many2One('transport.contract', 'Contract', required=True,
        ondelete='CASCADE')
    equipment = fields.Many2One('maintenance.equipment', 'Vehicle',
        required=True, domain=[
            ('type_device', '=', 'vehicle'),
        ])
    agreement = fields.Boolean('Agreement')
    partner = fields.Many2One('party.party', 'Partner', depends=['agreement'],
        states={
            'required': Eval('agreement', False),
            'invisible': ~Eval('agreement', False),
        })
    kind = fields.Selection([
            ('temporal_union', 'Union Temporal'),
            ('agreement', 'Agreement'),
            ('', ''),
        ], 'Kind')
    kind_string = kind.translated('kind')

    def get_rec_name(self, name):
        rec_name = self.equipment.product.name
        return (rec_name)


class ContractExtractDriver(ModelSQL):
    "Contract Extract - Transport Driver"
    __name__ = "transport.contract.extract-transport.driver"
    _table = 'transport_extract_transport_driver_rel'
    extract = fields.Many2One('transport.contract.extract',
            'Extract', required=True, ondelete='CASCADE')
    driver = fields.Many2One('transport.driver', 'Driver',
            required=True, ondelete='RESTRICT')


class ContractRequirement(ModelSQL, ModelView):
    "Contract Requirement"
    __name__ = "transport.contract.requirement"
    contract = fields.Many2One('transport.contract', 'Contract',
            ondelete='CASCADE', required=True)
    requirement = fields.Many2One('transport.requirement', 'Requirement',
            required=True)
    accomplish = fields.Boolean('Accomplish')


class ContractRoute(ModelSQL, ModelView):
    "Contract Route"
    __name__ = "transport.contract.route"
    contract = fields.Many2One('transport.contract', 'Contract',
        ondelete='CASCADE', required=True)
    name = fields.Char('Route', required=True)


class TransportContractReport(Report):
    __name__ = 'transport_contract.contract_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        report_context['company'] = Company(company_id)
        return report_context


class AutoGenerateExtractStart(ModelView):
    "Auto Generate Contract Extract"
    __name__ = 'transport.auto_generate_extract.start'
    contract = fields.Many2One('transport.contract', 'Contract', required=True)
    start_sequence = fields.Integer('Start Sequence', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    vehicles = fields.Many2Many('transport.contract.vehicle', None, None,
        'Vehicles', domain=[
            ('contract', '=', Eval('contract')),
        ])


class AutoGenerateExtract(Wizard):
    "Auto Generate Contract Extract"
    __name__ = 'transport.auto_generate_extract'
    start = StateView('transport.auto_generate_extract.start',
            'transport.auto_generate_extract_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Contract = pool.get('transport.contract')
        Extract = pool.get('transport.contract.extract')
        Driver = pool.get('transport.driver')
        drivers = Driver.search([])

        drivers_dict = {}

        for d in drivers:
            if not d.code or d.code == '':
                continue
            if d.code not in drivers_dict:
                drivers_dict[d.code] = []
            drivers_dict[d.code].append(d)

        extract_to_create = []
        sequence = self.start.start_sequence
        contract = Contract(self.start.contract)
        for v in self.start.vehicles:
            for drivers in drivers_dict.values():
                sequence_string = str(sequence).zfill(4)

                if len(sequence_string) > 4:
                    sequence_string = sequence_string[-4:]

                number = contract.resolution + contract.number + sequence_string
                extract_to_create.append({
                    'contract': self.start.contract,
                    'start_date': self.start.start_date,
                    'end_date': self.start.end_date,
                    'sequence': sequence_string,
                    'vehicle': v,
                    'number': number,
                    'drivers': [('add', drivers)],
                })
                sequence += 1
        Extract.create(extract_to_create)
        return 'end'


class PrintContractExtractStart(ModelView):
    "Print Contract Extract"
    __name__ = 'transport.print_contract_extract.start'
    extract = fields.Many2One('transport.contract.extract',
            'Extract', required=True)


class PrintContractExtract(Wizard):
    "Print Contract Extract"
    __name__ = 'transport.print_contract_extract'
    start_state = 'search_vehicle'
    search_vehicle = StateTransition()
    start = StateView('transport.print_contract_extract.start',
        'transport.print_contract_extract_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateAction('transport.report_transport_contract_extract')

    def transition_search_vehicle(self):
        Extract = Pool().get('transport.print_contract_extract.start')
        domain = [
                ('contract', '=', Transaction().context.get('active_id')),
        ]
        Extract.extract.domain = domain
        return 'start'

    def do_print_(self, action):
        data = {
            'extract': self.start.extract.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PrintContractExtractReport(Report):
    __name__ = 'transport.contract_extract_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Extract = Pool().get('transport.contract.extract')
        if data.get('extract'):
            extract_id = data['extract']
            records = [Extract(extract_id)]
        report_context['records'] = records
        return report_context


class PrintContractExtractMarkedReport(PrintContractExtractReport):
    __name__ = 'transport.contract_extract_report_marked'
