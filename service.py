# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, datetime, time, timedelta
from decimal import Decimal

from dateutil import tz
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Eval, Get, Id, If, In
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import (
    Button,
    StateAction,
    StateReport,
    StateTransition,
    StateView,
    Wizard,
)

STATES = {
    'readonly': (Eval('state') != 'draft'),
}


from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Bogota')

_ZERO = Decimal('0.0')


class Service(Workflow, ModelSQL, ModelView):
    "Transport Service"
    __name__ = 'transport.service'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES, domain=[('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0))])
    equipment = fields.Many2One('maintenance.equipment', 'Vehicle',
            required=True, states=STATES, domain=[
                ('type_device', '=', 'vehicle'),
            ])
    service_product = fields.Many2One('product.product', 'Service', domain=[
        ('type', '=', 'service'),
        ('template.salable', '=', True),
        ], required=True, states=STATES)
    driver1 = fields.Many2One('party.party', 'Driver One',
            states=STATES)
    driver2 = fields.Many2One('party.party', 'Driver Two',
            states=STATES)
    party = fields.Many2One('party.party', 'Customer', states=STATES)
    unit_price = fields.Numeric('Unit Price', digits=(16, 2))
    classification = fields.Many2One('transport.classification',
            'Classification', states=STATES)
    schedule_date = fields.Date('Schedule Date', states=STATES)
    start_date_planned = fields.DateTime('Start Date Planned',
            states=STATES)
    end_date_planned = fields.DateTime('End Date Planned',
            states=STATES)
    start_date_effective = fields.DateTime('Start Date Effective',
            states=STATES)
    end_date_effective = fields.DateTime('End Date Effective',
            states=STATES)
    origin = fields.Many2One('transport.place', 'Origin',
            states=STATES)
    destination = fields.Many2One('transport.place',
            'Destination', states=STATES)
    shift = fields.Many2One('transport.shift', 'Shift', states=STATES)
    route = fields.Many2One('transport.route', 'Route', states=STATES)
    sale = fields.Many2One('sale.sale', 'Sale Order', states=STATES)
    currency = fields.Many2One('currency.currency', 'Currency', required=True,
        states={
            'readonly': ((Eval('state') != 'draft')
                | (Eval('cost_lines', [0]) & Eval('currency', 0))),
            },
        depends=['state'])
    fuel_comsumption = fields.Function(fields.Float('Fuel Comsumption'),
            'get_fuel_comsumption')
    fuel_unit_price = fields.Function(fields.Numeric('Fuel Unit Price',
            digits=(16, 2)), 'get_fuel_unit_price')
    hour_machine_unit_price = fields.Function(fields.Numeric('Hour Machine Unit Price',
            digits=(16, 2)), 'get_hour_machine_unit_price')
    start_odometer = fields.Integer('Start Odometer', states=STATES)
    end_odometer = fields.Integer('End Odometer', states=STATES)
    occupation = fields.Integer('Occupation', states=STATES)
    rest_time = fields.Float('Rest Time', states=STATES)
    comment = fields.Text('Comment', states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),
            ('in_operation', 'In Operation'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    cost_lines = fields.One2Many('transport.service.cost_line',
            'service', 'Cost Lines', states=STATES)
    route_lines = fields.One2Many('transport.service.route_line',
            'service', 'Route Lines', states=STATES)
    distance = fields.Function(fields.Float('Distance'),
            'on_change_with_distance')
    total_time = fields.Function(fields.Float('Total Time',
            digits=(16, 2)), 'on_change_with_total_time')
    fuel_index = fields.Function(fields.Float('Fuel Index', digits=(16, 2),
            ), 'on_change_with_fuel_index')
    cost_by_distance = fields.Function(fields.Numeric('Cost By Distance'),
            'on_change_with_cost_by_distance')
    total_cost = fields.Function(fields.Numeric('Total Cost',
            digits=(16, 2)), 'on_change_with_total_cost')
    hour_machine = fields.Function(fields.Float('Hour Machine'),
            'get_hour_machine')
    driver_access = fields.Many2One('staff.access', 'Access', states=STATES)

    @classmethod
    def __setup__(cls):
        super(Service, cls).__setup__()
        cls._order = [
            ('schedule_date', 'DESC'),
            ('number', 'DESC'),
        ]
        cls._transitions |= set((
                ('draft', 'assigned'),
                ('draft', 'cancel'),
                ('assigned', 'draft'),
                ('assigned', 'in_operation'),
                ('in_operation', 'assigned'),
                ('in_operation', 'done'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'assigned',
            },
            'assign': {
                'invisible': ~Eval('state').in_(['draft', 'in_operation']),
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'in_operation': {
                'invisible': Eval('state') != 'assigned',
            },
            'done': {
                'invisible': Eval('state') != 'in_operation',
            },
        })

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('number',) + tuple(clause[1:]),
            ('equipment',) + tuple(clause[1:]),
            ('driver1',) + tuple(clause[1:]),
        ]

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['number'] = None
        default['state'] = 'draft'
        default['driver_access'] = None
        new_records = []
        for record in records:
            new_record, = super(Service, cls).copy(
                    [record], default=default,
                    )
            new_records.append(new_record)
        return new_records

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            return Company(company).currency.id

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('assigned')
    def assign(cls, records):
        cls.set_number(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('in_operation')
    def in_operation(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        for record in records:
            pass

    @classmethod
    def validate(cls, services):
        for service in services:
            service.check_dates()

    def check_dates(self):
        if self.start_date_planned and self.end_date_planned and \
            self.start_date_planned > self.end_date_planned:
            raise UserError(gettext('transport.msg_invalid_dates'))
        if self.start_date_effective and self.end_date_effective and \
            self.start_date_effective > self.end_date_effective:
            raise UserError(gettext('transport.msg_invalid_dates'))

    @classmethod
    def set_number(cls, requests):
        """
        Fill the number field with the service sequence
        """
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('transport.configuration')
        config = Config(1)

        for request in requests:
            if request.number:
                continue
            if not config.transport_service_sequence:
                continue
            # number = Sequence.get_id(config.transport_service_sequence.id)
            number = config.transport_service_sequence.get()
            cls.write([request], {'number': number})

    # @fields.depends('route', 'cost_lines')
    # def on_change_route(self, name=None):
    #     if self.route:
    #         CostLine = Pool().get('transport.service.cost_line')
    #         for line in self.route:

    @fields.depends('equipment', 'service_product')
    def on_change_equipment(self, name=None):
        if self.equipment and hasattr(self.equipment, 'services'):
            if self.equipment.services:
                self.service_product = self.equipment.services[0].id

    @fields.depends('start_odometer', 'end_odometer')
    def on_change_with_distance(self, name=None):
        if self.start_odometer and self.end_odometer:
            return round(self.end_odometer - self.start_odometer, 2)

    @fields.depends('start_date_effective', 'end_date_effective')
    def on_change_start_date_effective(self, name=None):
        if self.start_date_effective and not self.end_date_effective:
            self.end_date_effective = self.start_date_effective

    @fields.depends('start_date_effective', 'end_date_effective')
    def on_change_with_total_time(self, name=None):
        if self.start_date_effective and self.end_date_effective:
            delta = self.end_date_effective - self.start_date_effective
            value = round(delta.total_seconds() / 3600.0, 2)
            return value

    @fields.depends('fuel_comsumption', 'distance')
    def on_change_with_fuel_index(self, name=None):
        if self.fuel_comsumption and self.distance and self.distance > 0:
            return round(self.fuel_comsumption / self.distance, 2)

    @fields.depends('currency', 'distance', 'total_cost')
    def on_change_with_cost_by_distance(self, name=None):
        if self.currency and self.distance and self.total_cost and self.distance > 0:
            return self.currency.round(self.total_cost / Decimal(self.distance))

    @fields.depends('cost_lines')
    def on_change_with_total_cost(self, name=None):
        res = _ZERO
        for line in self.cost_lines:
            res += line.amount or _ZERO
        return res

    def get_fuel_comsumption(self, name=None):
        cat_fuel = Id('transport', 'cat_fuel').pyson()
        quantity = 0
        for line in self.cost_lines:
            if line.product.account_category and line.product.account_category.id == cat_fuel:
                quantity += line.quantity
        return quantity

    def get_fuel_unit_price(self, name=None):
        cat_fuel = Id('transport', 'cat_fuel').pyson()
        avg_unit_price = _ZERO
        sum_unit_price = _ZERO
        count = 0
        for line in self.cost_lines:
            if line.product.account_category and line.product.account_category.id == cat_fuel:
                count += 1
                sum_unit_price += line.unit_price
        if count > 0:
            avg_unit_price = sum_unit_price / count
        return avg_unit_price

    def get_hour_machine_unit_price(self, name=None):
        cat_hour_machine = Id('transport', 'cat_hour_machine').pyson()
        price = _ZERO
        for line in self.cost_lines:
            if line.product.account_category and line.product.account_category.id == cat_hour_machine:
                price = line.unit_price
        return price

    def get_hour_machine(self, name=None):
        cat_hour_machine = Id('transport', 'cat_hour_machine').pyson()
        res = 0
        for line in self.cost_lines:
            if line.product.account_category and line.product.account_category.id == cat_hour_machine:
                res += line.quantity
        return res


class ServiceRouteLine(ModelSQL, ModelView):
    "Transport Service Route Line"
    __name__ = 'transport.service.route_line'
    service = fields.Many2One('transport.service', 'Service',
        ondelete='CASCADE', required=True)
    sequence = fields.Char('Sequence')
    occupation = fields.Integer('Occupation')
    route = fields.Many2One('transport.route', 'Route', required=True)
    kind = fields.Selection([
            ('enter', 'Enter'),
            ('exit', 'Exit'),
        ], 'Kind', required=True)
    arrival_date = fields.Time('Arrival Time')
    departure_date = fields.Time('Departure Time')
    notes = fields.Char('Notes')
    origin = fields.Many2One('transport.place', 'Origin')
    destination = fields.Many2One('transport.place', 'Destination')

    @staticmethod
    def default_kind():
        return 'enter'

    @classmethod
    def __setup__(cls):
        super(ServiceRouteLine, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))


class ServiceCostLine(ModelSQL, ModelView):
    "Service Cost Line"
    __name__ = 'transport.service.cost_line'
    service = fields.Many2One('transport.service', 'Service',
            ondelete='CASCADE', required=True)
    product = fields.Many2One('product.product', 'Product',
            required=True)
    quantity = fields.Float('Quantity', digits=(16, 2),
            required=True)
    cost_type = fields.Selection([
                ('manual', 'Manual'),
                ('origin', 'Origin'),
            ], 'Cost Type', required=True)
    origin = fields.Reference('Origin', selection='get_origin',
        states={
            'readonly': Eval('state') != 'draft',
            }, depends=['state'])
    unit_price = fields.Numeric('Unit Price', digits=(16, 2), required=True)
    amount = fields.Function(fields.Numeric('Amount', digits=(16, 2)),
            'get_amount')
    notes = fields.Text('Notes')

    @classmethod
    def __setup__(cls):
        super(ServiceCostLine, cls).__setup__()
        cls._order.insert(0, ('id', 'ASC'))

    @classmethod
    def _get_origin(cls):
        "Return list of Model names for origin Reference"
        return ['transport.service.cost_line']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
                ('model', 'in', models),
                ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @staticmethod
    def default_cost_type():
        return 'manual'

    def get_amount(self, name):
        return self.on_change_with_amount()

    @fields.depends('quantity', 'unit_price')
    def on_change_with_amount(self):
        if self.quantity and self.unit_price:
            return Decimal(self.quantity) * self.unit_price
        return Decimal('0.0')


class TransportShift(Workflow, ModelSQL, ModelView):
    "Transport Service"
    __name__ = 'transport.shift'
    name = fields.Char('Name')


class ServiceReport(Report):
    __name__ = 'transport.service'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(ServiceReport, cls).get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class ServicesSummaryStart(ModelView):
    "Services Summary Start"
    __name__ = 'transport.services_summary.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ServicesSummary(Wizard):
    "Services Summary"
    __name__ = 'transport.services_summary'
    start = StateView('transport.services_summary.start',
        'transport.services_summary_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateAction('transport.report_services_summary')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ServicesSummaryReport(Report):
    __name__ = 'transport.services_summary.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(ServicesSummaryReport, cls).get_context(records, header, data)

        pool = Pool()
        Services = pool.get('transport.service')
        Company = pool.get('company.company')

        total_costs = []
        fuel_comsumption = []
        distance = []
        total_time = []
        hour_machine = []

        start_date = datetime.combine(data['start_date'], time(0, 0, 0))
        end_date = datetime.combine(data['end_date'], time(23, 59, 0))

        dom_services = [
            ('start_date_effective', '>=', start_date),
            ('start_date_effective', '<=', end_date),
            ('state', 'in', ('assigned', 'in_operation', 'done')),
        ]

        services = Services.search(dom_services)
        for service in services:
            total_costs.append(service.total_cost)
            fuel_comsumption.append(service.fuel_comsumption)
            if service.distance:
                distance.append(service.distance)
            total_time.append(service.total_time)
            hour_machine.append(service.hour_machine)

        report_context['records'] = services
        report_context['company'] = Company(data['company'])
        report_context['total_costs'] = sum(total_costs)
        report_context['fuel_comsumption'] = sum(fuel_comsumption)
        report_context['hour_machine'] = sum(hour_machine)
        report_context['distance'] = sum(distance)
        report_context['total_time'] = sum(total_time)
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        return report_context


class ServiceForceDraft(Wizard):
    "Service Force Draft"
    __name__ = 'transport.service.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        if ids:
            ids = str(ids[0])
            query = "UPDATE transport_service SET state='draft' WHERE id=%s"
            cursor.execute(query % ids)
        return 'end'


class ServicesDailyStart(ModelView):
    "Services Daily Start"
    __name__ = 'transport.services_daily.start'
    start_date = fields.Date('Start Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    report = fields.Many2One('ir.action.report', 'Report',
        domain=[('report_name', 'in', [
                'transport.services_daily_report',
                'transport.services_driver_report',
                'transport.services_shift_report',
            ])], required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ServicesDaily(Wizard):
    "Services Daily"
    __name__ = 'transport.services_daily'
    start = StateView('transport.services_daily.start',
            'transport.print_services_daily_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('transport.services_daily_report')

    def do_print_(self, action):
        company = self.start.company
        data = {
            'date': self.start.start_date,
            'company': company.id,
        }

        action['report'] = self.start.report.report
        action['report_name'] = self.start.report.report_name
        action['id'] = self.start.report.id
        action['action'] = self.start.report.action.id
        return action, data

    def transition_print_(self):
        return 'end'


class ServicesDailyReport(Report):
    __name__ = 'transport.services_daily_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(ServicesDailyReport, cls).get_context(records, header, data)
        MAX_DAYS = 31

        pool = Pool()
        Company = pool.get('company.company')
        Service = pool.get('transport.service')

        start_date = datetime.combine(data['date'], time(0, 0))
        limit_date = start_date + timedelta(days=MAX_DAYS - 1, minutes=1439)
        start_date = Company.convert_timezone(start_date, True)
        limit_date = Company.convert_timezone(limit_date, True)

        services = Service.search([
            ('start_date_effective', '>=', start_date),
            ('start_date_effective', '<=', limit_date),
        ])

        alldays = {}
        alldays_convert = {}
        date_ = data['date']
        data['total_revenue'] = float(0)
        data['total_days'] = float(0)
        for nd in range(MAX_DAYS + 1):
            day_n = 'day' + str(nd + 1)
            tdate = date_ + timedelta(nd)
            data[day_n] = tdate
            data['total_' + day_n] = []
            data[('revenue_' + day_n)] = 0
            data[('rate_' + day_n)] = 0
            data[('count_' + day_n)] = 0
            alldays[day_n] = ''
            alldays_convert[tdate] = day_n

        dservices = {}

        for service in services:
            if service.equipment.id not in dservices:
                # FIXME: service.vehicle
                dservices[service.equipment.id] = {
                    'name': service.equipment.rec_name,
                    'total_vehicle': float(0),
                    'category': service.service_product.name if service.service_product else '',
                }
                dservices[service.equipment.id].update(alldays.copy())
            date_tz = Company.convert_timezone(service.start_date_effective).date()
            dayn = alldays_convert[date_tz]
            total_time = service.total_time or 0.0
            if dservices[service.equipment.id][dayn] == '':
                dservices[service.equipment.id][dayn] = 0.0
                data[('count_' + dayn)] += 1
            dservices[service.equipment.id][dayn] += total_time
            dservices[service.equipment.id]['total_vehicle'] += total_time
            data['total_' + dayn].append(total_time)
            amount = float(_ZERO)
            if service.service_product.list_price > _ZERO:
                amount = float(service.service_product.list_price) / 1000000
            data['revenue_' + dayn] += amount
            data['total_revenue'] += amount
            data['total_days'] += total_time

        for i in range(MAX_DAYS + 1):
            day_n = 'day' + str(i + 1)
            sum_total_day = sum(data['total_' + day_n])
            if data[('count_' + day_n)] > 0:
                rate = float(sum_total_day) / float(data[('count_' + day_n)])
            else:
                rate = 0
            data['total_' + day_n] = sum_total_day
            data['rate_' + day_n] = rate

        # FIXME
        # records_sorted = [value for (key, value) in sorted(
        #    [(v['name'], v) for v in dservices.values()])]

        report_context['records'] = dservices.values()
        report_context['date'] = data['date']
        report_context['company'] = Company(data['company']).party.name
        return report_context


class ServicesShiftReport(Report):
    "Services Shift Report"
    __name__ = 'transport.services_shift_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(ServicesShiftReport, cls).get_context(records, header, data)
        MAX_DAYS = 31
        pool = Pool()
        Company = pool.get('company.company')
        Service = pool.get('transport.service')

        start_date = datetime.combine(data['date'], time(0, 0))
        limit_date = start_date + timedelta(days=MAX_DAYS)
        start_date = Company.convert_timezone(start_date, True)
        limit_date = Company.convert_timezone(limit_date, True)
        services = Service.search([
            ('start_date_effective', '>=', start_date),
            ('start_date_effective', '<', limit_date),
        ])
        alldays = {}
        total_alldays = {}
        alldays_convert = {}
        date_ = data['date']
        data['total_days'] = float(0)
        for nd in range(MAX_DAYS):
            day_n = 'day' + str(nd + 1)
            tdate = date_ + timedelta(nd)
            data[day_n] = tdate
            data['total_' + day_n] = []
            alldays[day_n] = ''
            total_alldays['total_' + day_n] = 0
            alldays_convert[tdate] = day_n

        dservices = {}

        for service in services:
            if not service.shift:
                continue
            if service.driver1.id not in dservices:
                dservices[service.driver1.id] = {
                    'name': service.driver1.name,
                    'shifts': {},
                }
                dservices[service.driver1.id].update(total_alldays.copy())
            if service.shift.id not in dservices[service.driver1.id]['shifts'].keys():
                dservices[service.driver1.id]['shifts'][service.shift.id] = {
                    'name': service.shift.name,
                }
                dservices[service.driver1.id]['shifts'][service.shift.id].update(alldays.copy())

            date_tz = Company.convert_timezone(
                service.start_date_effective).date()
            dayn = alldays_convert[date_tz]
            total_time = service.total_time or 0.0
            dservices[service.driver1.id]['shifts'][service.shift.id][dayn] = total_time
            dservices[service.driver1.id]['total_' + dayn] += total_time

        report_context['records'] = dservices.values()
        report_context['date'] = data['date']
        report_context['company'] = Company(data['company']).party.name
        return report_context


class ServicesDriverReport(Report):
    "Services Driver Report"
    __name__ = 'transport.services_driver_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(ServicesDriverReport, cls).get_context(records, header, data)
        MAX_DAYS = 31
        pool = Pool()
        Company = pool.get('company.company')

        start_date = datetime.combine(data['date'], time(0, 0))
        limit_date = start_date + timedelta(days=MAX_DAYS)
        start_date = Company.convert_timezone(start_date, True)
        limit_date = Company.convert_timezone(limit_date, True)
        services = Service.search([
            ('start_date_effective', '>=', start_date),
            ('start_date_effective', '<', limit_date),
            ('driver1', '!=', None),
        ])
        alldays = {}
        alldays_convert = {}
        date_ = data['date']
        data['total_days'] = float(0)
        for nd in range(MAX_DAYS):
            day_n = 'day' + str(nd + 1)
            tdate = date_ + timedelta(nd)
            data[day_n] = tdate
            data['total_' + day_n] = []
            data[('count_' + day_n)] = 0
            alldays[day_n] = ''
            alldays_convert[tdate] = day_n

        dservices = {}

        for service in services:
            if service.driver1.id not in dservices:
                dservices[service.driver1.id] = {
                    'name': service.driver1.name,
                    'total_driver': float(0),
                    'total_rest': MAX_DAYS,
                }
                dservices[service.driver1.id].update(alldays.copy())

            date_tz = Company.convert_timezone(service.start_date_effective).date()
            dayn = alldays_convert[date_tz]
            total_time = service.total_time or 0.0
            if dservices[service.driver1.id][dayn] == '':
                dservices[service.driver1.id][dayn] = 0.0
                data[('count_' + dayn)] += 1

            dservices[service.driver1.id][dayn] += total_time
            dservices[service.driver1.id]['total_driver'] += total_time
            data['total_' + dayn].append(total_time)
            data['total_days'] += total_time

        for driver in dservices.values():
            for i in range(1, MAX_DAYS + 1):
                if driver['day' + str(i)] != '' and driver['day' + str(i)] > 0:
                    driver['total_rest'] -= 1

        for i in range(MAX_DAYS):
            day_n = 'day' + str(i + 1)
            sum_total_day = sum(data['total_' + day_n])
            if data[('count_' + day_n)] > 0:
                rate = float(sum_total_day) / float(data[('count_' + day_n)])
            else:
                rate = 0
            data['total_' + day_n] = sum_total_day
            data['rate_' + day_n] = rate

        records_sorted = [value for (key, value) in sorted(
            [(v['name'], v) for v in dservices.values()])]
        report_context['records'] = records_sorted
        report_context['date'] = data['date']
        report_context['company'] = Company(data['company']).party.name
        return report_context


class DriverAccessSyncStart(ModelView):
    "Driver Access Sync Start"
    __name__ = 'transport.driver_access_sync.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    driver = fields.Many2One('party.party', 'Driver')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class DriverAccessSync(Wizard):
    "Services Driver"
    __name__ = 'transport.driver_access_sync'
    start = StateView('transport.driver_access_sync.start',
            'transport.driver_access_sync_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Access = pool.get('staff.access')
        Service = pool.get('transport.service')
        Employee = pool.get('company.employee')
        start_date = datetime.combine(self.start.start_date, time(0, 0, 0))
        end_date = datetime.combine(self.start.end_date, time(23, 59, 0))

        services_dom = [
            ('start_date_effective', '>=', start_date),
            ('end_date_effective', '<=', end_date),
            ('driver_access', '=', None),
        ]
        if self.start.driver:
            services_dom.append(('driver1', '=', self.start.driver))
        services = Service.search(services_dom)
        values = {}
        drivers2employee = {}

        employees_dom = []
        if self.start.driver:
            employees_dom.append(('party', '=', self.start.driver))

        employees = Employee.search(employees_dom)
        drivers2employee = dict([(employee.party.id, employee.id) for employee in employees])

        # TODO Add grouping service by driver and write all services use map please

        for service in services:
            if not service.driver1:
                continue
            employee_id = drivers2employee.get(service.driver1.id)
            if not employee_id:
                continue

            start_date_effective = service.start_date_effective.replace(tzinfo=from_zone)
            start_date_effective = start_date_effective.astimezone(to_zone)

            date_start = start_date_effective.date()
            if employee_id not in values:
                values[employee_id] = {}
            if date_start not in values[employee_id].keys():
                values[employee_id][date_start] = []
            values[employee_id][date_start].append(
                    (service, service.start_date_effective, service.end_date_effective))

        for employee in values:
            for date_ in values[employee]:
                start_rest = None
                end_rest = None
                rest = Decimal(0)
                res = values[employee][date_]
                if not res:
                    continue

                timestamps = []
                service_to_write = []
                if len(res) <= 2:
                    for r in res:
                        service = r[0]
                        timestamps.extend(r[1:3])
                        service_to_write.append(service)
                elif len(res) > 2 and len(res) < 5:
                    data = []
                    for r in res:
                        service = r[0]
                        data.append((r[1], r[2]))
                        service_to_write.append(service)
                    data = sorted(data)
                    data = self.equales(data)
                    data = self.combine(data)
                    for i in data:
                        for j in i:
                            for k in j:
                                timestamps.append(k)
                elif len(res) > 4:
                    continue
                enter_timestamp = min(timestamps)
                exit_timestamp = max(timestamps)
                timestamps = sorted(timestamps)

                if len(timestamps) == 4:
                    if timestamps[1] != timestamps[2]:
                        start_rest = timestamps[1]
                        end_rest = timestamps[2]
                elif len(timestamps) not in (2, 4):
                    continue
                if start_rest and end_rest:
                    if start_rest > end_rest:
                        raise UserError(gettext(
                            'transport.msg_wrong_date_day',
                            date_=service.date_,
                            driver=service.driver1.name,
                        ))
                    rest = Access().compute_timedelta(start_rest, end_rest)

                access, = Access.create([{
                    'employee': employee,
                    'enter_timestamp': enter_timestamp,
                    'exit_timestamp': exit_timestamp,
                    'start_rest': start_rest,
                    'end_rest': end_rest,
                    'rest': rest,
                }])
                service.write(service_to_write, {'driver_access': access.id})
        return 'end'

    def equales(self, values):
        data = []
        if len(values) > 2:
            p = (None, None)
            subdata = []
            for j in values:
                if p[0] == None:
                    p = (j[0], j[1])
                    subdata.append((j[0], j[1]))
                    continue
                if j[0] == p[1]:
                    subdata.pop()
                    val1, val2 = p[0], j[1]
                else:
                    val1, val2 = j[0], j[1]
                subdata.append((val1, val2))
                p = (val1, val2)
            data.append(subdata)
        return data

    def combine(self, values):
        data = []
        for i in values:
            subdata = []
            if len(i) > 2 and len(i) < 5:
                count = 0
                for j in i:
                    count += 1
                    if count == 1:
                        subdata.append((j[0], j[1]))
                        continue
                    if count == 2:
                        delta = j[1] - j[0]
                        p = subdata.pop()
                        val1, val2 = p[0], p[1] + delta
                    elif count == 3:
                        val1, val2 = j[0], j[1]
                    elif count == 4:
                        p = subdata.pop()
                        delta = p[1] - p[0]
                        val1, val2 = j[0] - delta, j[1]
                    else:
                        pass
                    subdata.append((val1, val2))
                data.append(subdata)
            else:
                data.append(i)
        return data


class CreateSaleFromService(Wizard):
    "Create Sale From Services"
    __name__ = 'transport.service.create_sale'
    """
    this is the wizard that allows create a sale from services executed.
    """
    start_state = 'create_sale'
    create_sale = StateTransition()

    def transition_create_sale(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Service = pool.get('transport.service')
        ids = Transaction().context['active_ids']
        company_id = Transaction().context['company']
        Party = pool.get('party.party')
        if not ids:
            return 'end'

        services = Service.search([
            ('id', 'in', ids),
            ('state', 'in', ['done', 'in_operation']),
            ('sale', '=', None),
        ])
        parties = [s.party for s in services if s.party]
        if not parties:
            return 'end'
        parties = list(set(parties))
        today = date.today()

        party_sale = {}
        for party in parties:

            if party.id not in party_sale:
                sale = Sale(
                    company=company_id,
                    party=party.id,
                    price_list=None,
                    sale_date=today,
                    state='draft',
                    # origin=self,
                    invoice_address=Party.address_get(party, type='invoice'),
                    shipment_address=Party.address_get(party, type='delivery'),
                )
                sale.save()
                party_sale[party.id] = sale

        for service in services:
            if not service.party:
                continue

            sale = party_sale.get(service.party.id)
            new_line = {
                'sale': sale.id,
                'type': 'line',
                'unit': service.service_product.template.default_uom.id,
                'quantity': 1,
                'unit_price': service.unit_price,
                'product': service.service_product.id,
                'description': service.service_product.rec_name,
            }
            SaleLine.create([new_line])
            service.sale = sale
            service.save()
        return 'end'
